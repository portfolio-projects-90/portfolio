/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{js, jsx, ts, tsx}'
  ],
  theme: {
    screens: {
      'phone': '450px',
      // => @media (min-width: 640px) { ... }

      'tablet': '640px',
      // => @media (min-width: 640px) { ... }

      'laptop': '1024px',
      // => @media (min-width: 1024px) { ... }

      'desktop': '1280px',
      // => @media (min-width: 1280px) { ... }
    },
    extend: {
      height: {
        '1': '1vh',
        '2': '2vh',
        '3': '3vh',
        '4': '4vh',
        '5': '5vh',
        '6': '6vh',
        '7': '7vh',
        '8': '8vh',
        '9': '9vh',
        '10': '10vh',
        '11': '11vh',
        '12': '12vh',
        '13': '13vh',
        '14': '14vh',
        '15': '15vh',
        '20': '20vh',
        '25': '25vh',
        '30': '30vh',
        '40': '40vh',
        '50': '50vh',
        '60': '60vh',
        '70': '70vh',
        '80': '80vh',
        '85': '85vh',
        '90': '90vh',
        '100': '100vh',
        '110': '110vh',
        '120': '120vh',
        '130': '130vh',
        '140': '140vh',
        '150': '150vh',
        '1px': '1px',
        '2px': '2px',
        '3px': '3xp',
        '4px': '4px',
        '5px': '5px',
        '7px': '7px',
        '10px': '10px'
      },
      width: {
        '1': '1vw',
        '2': '2vw',
        '3': '3vw',
        '4': '4vw',
        '5': '5vw',
        '6': '6vw',
        '7': '7vw',
        '8': '8vw',
        '9': '9vw',
        '10': '10vw',
        '11': '11vw',
        '12': '12vw',
        '13': '13vw',
        '14': '14vw',
        '15': '15vw',
        '20': '20vw',
        '25': '25vw',
        '30': '30vw',
        '35': '35vw',
        '40': '40vw',
        '50': '50vw',
        '60': '60vw',
        '70': '70vw',
        '80': '80vw',
        '90': '90vw',
        '100': '100vw',
        '110': '110vw',
        '120': '120vw',
        '130': '130vw',
        '140': '140vw',
        '150': '150vw',
        '2p': '2%',
        '3p': '3%',
        '5p': '5%',
        '10p': '10%',
        '15p': '15%',
        '20p': '20%',
        '30p': '30%',
        '40p': '40%',
        '50p': '50%',
        '60p': '60%',
        '70p': '70%',
        '80p': '80%',
        '90p': '90%',
        '100p': '100%',
        '1px': '1px',
        '2px': '2px',
        '3px': '3xp',
        '4px': '4px',
        '5px': '5px',
        '7px': '7px',
        '10px': '10px'
      },
      extend: {
        spacing: {
          '40': '40vh'
        }
      }
    },
    colors: {
      'bggradient1': '#766dff',
      'bggradient2': '#88f3ff',
      'primarybg': '#777777',
      'alternatebg1': '#ffffff',
      'alternatebg2': '#000000',
      'red': 'red',
      'blue': 'blue',
      'yellow': 'yellow',
      'green': 'green',
      'whitesmoke': 'whitesmoke',
      'black': 'black',
      'purple': 'purple',
      'white': 'white',
      'mostborder': '#eeeeee',
      'mostborder1': '#ebe8e8',
      'progressbarbg': '#dcdee1',
      'bgalt3': '#f9f9ff;',
      'btnbg': 'rgba(255, 255, 255, 0.1)'
    },
    boxShadow: {
      'profileshadow': '0px 20px 80px 0px rgba(153, 153, 153, 0.3)'
    }
  },
  plugins: [],
}

