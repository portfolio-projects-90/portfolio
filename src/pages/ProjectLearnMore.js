import HeaderTextModule from "../modules/HeaderTextModule"
import { useSelector } from 'react-redux'
import { useState, useEffect } from "react";
import { codeProjects, noCodeProjects } from "../dataset/ProjectsData"

function ProjectLearnMore() {

    const projectType = useSelector((state) => state.projectProerties.projectType);
    const projectCode = useSelector((state) => state.projectProerties.projectCode);

    const [details, setDetails] = useState({});
    const [documentation, setDocumentation] = useState([]);

    useEffect(() => {
        selectData();
      }, []);

      function selectData(){
        if(projectType === 'code'){
            codeProjects.forEach(function(value){
                if(value.code === projectCode){
                    setDetails(value);
                    setDocumentation(value.documentations);
                }
            })
        }else if(projectType === 'noCode'){
            noCodeProjects.forEach(function(value){
                if(value.code === projectCode){
                    setDetails(value);
                    setDocumentation(value.documentations);
                }
            })
        }
      }
    
  return (
    <div className='min-h-screen flex flex-col gap-10 px-5 laptop:px-40 pt-20 pb-40'>
        <HeaderTextModule title={details.title} content={details.type} />

        <div className='flex flex-row justify-center'>
            <img className='h-40 laptop:h-80 w-full laptop:w-50 rounded-xl' alt='Pic' src='https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg' />
        </div>
        <div className='flex flex-col gap-5'>
            <p className='uppercase text-center text-2xl text-black font-bold'>Tools Used</p>
            <p className='text-base text-black'>{details.toolsUsed}</p>
        </div>
        <div className='flex flex-col gap-5'>
            <p className='uppercase text-center text-2xl text-black font-bold'>Description</p>
            <p className='text-base text-black'>{details.description}</p>
        </div>
        <div className='flex flex-col gap-5'>
            <p className='uppercase text-center text-2xl text-black font-bold'>Documentations</p>
            <ul className='flex flex-col gap-5 list-disc pl-5'>
                {documentation.map((doc) => {
                    return(
                        <li>
                            <a href={doc.link} target='_blank' rel="noopener noreferrer">{doc.name}</a>
                        </li>
                    )
                })}
            </ul>
        </div>
    </div>
  )
}

export default ProjectLearnMore
