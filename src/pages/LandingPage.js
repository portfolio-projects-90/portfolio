import Header from "../components/landingpagecomponents/Header";
import Footer from "../components/landingpagecomponents/Footer";
import InwardLandingPage from "./InwardLandingPage";
import { Route, Routes } from 'react-router-dom';
import Features from "../components/landingpagecomponents/Features";
import ProjectLearnMore from "./ProjectLearnMore";

function LandingPage() {
  return (
    <div className="bg-alternatebg1s min-h-screen">

        <div className="sticky z-40 top-0 mx-auto bg-black laptop:bg-gradient-to-r from-bggradient1 to-bggradient2 h-10 tablet:h-10 laptop:h-15">

            <Header />

        </div>
        
        <div className="bg-white h-full w-full">

            <Routes>
                
                <Route exact path='/' element={<InwardLandingPage />} />

                <Route path='/projects' element={<Features />} />

                <Route path='/projects/learn/more' element={<ProjectLearnMore />} />

            </Routes>

        </div>
        
        <div className="bg-black min-h-50 w-full laptop:h-50">
            <Footer />
        </div>

    </div>
  )
}

export default LandingPage
