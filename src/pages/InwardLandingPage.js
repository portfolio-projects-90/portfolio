import ProfileSection from "../components/landingpagecomponents/ProfileSection";
import AboutMe from "../components/landingpagecomponents/AboutMe";
import Experiences from "../components/landingpagecomponents/Experiences";
import Offerings from "../components/landingpagecomponents/Offerings";
import Features from "../components/landingpagecomponents/Features";
import Testimoniasl from "../components/landingpagecomponents/Testimoniasl";
import { useEffect } from "react";
import { useDispatch } from 'react-redux'
import { setProjectToggle, setProjectTypeToggle, changeCodeCol, changeNoCodeCol } from '../redux/slice/ProjectProperties';

function InwardLandingPage() {
  
  const dispatch = useDispatch();

  useEffect(() => {
    changeProjectToggle();
  }, []);

  function changeProjectToggle(){
    dispatch(setProjectToggle(false));
    dispatch(setProjectTypeToggle(true));
    dispatch(changeCodeCol("text-bggradient1"));
    dispatch(changeNoCodeCol("text-black"));
  }

  return (
    <div className="w-full h-full">

      <ProfileSection />
      
      <div className="bg-white min-h-120 laptop:min-h-70 shadow-profileshadow laptop:shadow-transparent text-black px-5 tablet:px-10 laptop:px-40 flex flex-col self-center">
          <div className="tablet:h-50 laptop:h-40"></div>
          <AboutMe />
      </div>

      <div className="bg-gradient-to-r from-bggradient1 to-bggradient2 min-h-full">
          <Experiences />
      </div>

      <div className="bg-bgalt3 min-h-screen laptop:h-screen">
          <Offerings />
      </div>

      <div className="bg-white min-h-screen">
          <Features />
      </div>

      <div className="bg-bgalt3 h-screen w-full">
          <Testimoniasl />
      </div>
        
    </div>
    
  )
}

export default InwardLandingPage
