export const education = [
    {
        "duration": "November, 2014 - November, 2019",
        "company": "Federal University of Technology, Owerri",
        "rank": "Bacholor Electrical And Electronic Engineering",
        "location": "Owerri, Imo State, Nigeria"
    },
    {
        "duration": "June, 2013 - July, 2013",
        "company": "West African Exemination Council",
        "rank": "Senior School Certificate Examination",
        "location": "Sapele, Delta State, Nigeria"
    }
]

export const experience = [
    {
        "duration": "March, 2023 - Present",
        "company": "Systemspecs",
        "rank": "Mid-Level Backend Engineer",
        "location": "Lagos, Nigeria"
    },
    {
        "duration": "March, 2022 - December, 2022",
        "company": "IPEG",
        "rank": "Junior Backend Engineer",
        "location": "Lagos, Nigeria"
    }
]