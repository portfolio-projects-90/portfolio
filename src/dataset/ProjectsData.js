export const codeProjects = [
    {
        "code": "code1",
        "title": "Native Soup Website",
        "type": "Client Project",
        "projectCoverLink": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg",
        "toolsUsed": "If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each. If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.",
        "description": "If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each. If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.",
        "documentations": [
            {
                "name": "Platform Link",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            },
            {
                "name": "Git Repository",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            },
            {
                "name": "Postman Documentation",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            }
        ]
    },
    {
        "code": "code2",
        "title": "Food Delevery Website",
        "type": "Client Project",
        "projectCoverLink": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg",
        "toolsUsed": "If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each. If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.",
        "description": "If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each. If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.",
        "documentations": [
            {
                "name": "Platform Link",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            },
            {
                "name": "Git Repository",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            },
            {
                "name": "Postman Documentation",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            }
        ]
    },
    {
        "code": "code3",
        "title": "Sugar Plum",
        "type": "Client Project",
        "projectCoverLink": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg",
        "toolsUsed": "If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each. If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.",
        "description": "If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each. If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.",
        "documentations": [
            {
                "name": "Platform Link",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            },
            {
                "name": "Git Repository",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            },
            {
                "name": "Postman Documentation",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            }
        ]
    },
    {
        "code": "code4",
        "title": "Auto Sign",
        "type": "Client Project",
        "projectCoverLink": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg",
        "toolsUsed": "If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each. If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.",
        "description": "If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each. If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.",
        "documentations": [
            {
                "name": "Platform Link",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            },
            {
                "name": "Git Repository",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            },
            {
                "name": "Postman Documentation",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            }
        ]
    }
]

export const noCodeProjects = [
    {
        "code": "noCode1",
        "title": "Asa Odugwu",
        "type": "Client Project",
        "projectCoverLink": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg",
        "toolsUsed": "If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each. If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.",
        "description": "If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each. If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.",
        "documentations": [
            {
                "name": "Platform Link",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            }
        ]
    },
    {
        "code": "noCode2",
        "title": "Odugwuress",
        "type": "Client Project",
        "projectCoverLink": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg",
        "toolsUsed": "If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each. If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.",
        "description": "If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each. If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.",
        "documentations": [
            {
                "name": "Platform Link",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            }
        ]
    },
    {
        "code": "noCode3",
        "title": "Odugwu",
        "type": "Client Project",
        "projectCoverLink": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg",
        "toolsUsed": "If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each. If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.",
        "description": "If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each. If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.",
        "documentations": [
            {
                "name": "Platform Link",
                "link": "https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg"
            }
        ]
    }
]