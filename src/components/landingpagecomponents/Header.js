import { useDispatch } from 'react-redux'
import { setProjectToggle } from '../../redux/slice/ProjectProperties';
import { HashLink } from 'react-router-hash-link'
import { useState, useRef } from 'react';
import { Squash as Hamburger } from 'hamburger-react'
import { useClickAway } from 'react-use'

function Header() {

  const [isOpen, setOpen ] = useState(false);
  const ref = useRef(null);

  useClickAway(ref, () => setOpen(false));

  const dispatch = useDispatch();

  function changeProjectToggle(){
    dispatch(setProjectToggle(true));
    setOpen((prev) => !prev);
  }

  function changeOpen(){
    setOpen((prev) => !prev);
  }
  
  return (
    
    <nav className='mx-auto text-base text-alternatebg1 h-full flex flex-col laptop:flex-row justify-center items-center px-5 tablet:px-10 laptop:px-40 place-content-between'>
      
      <div className='flex flex-row w-full items-center place-content-between'>
        <div>
          <h1 className='text-lg font-bold'><a href='#/'>Apple</a></h1>
        </div>

        <div ref={ref} className='laptop:hidden justify-end flex flex-row'>
          <Hamburger toggled={isOpen} toggle={setOpen} size={20} />
        </div>
      </div>

      <div className='w-full hidden laptop:flex'>
        <div className=''>
          <ul className="items-center flex flex-col laptop:flex-row gap-y-10 py-10 laptop:py-0 laptop:gap-x-20">
            <li><a href='#/' onClick={() => changeOpen()}>HOME</a></li>
            <li><HashLink smooth to='/#about-me'>ABOUT</HashLink></li>
            <li><HashLink smooth to='/#offerings'>SERVICES</HashLink></li>
            <li><a href='#/projects' onClick={() => changeProjectToggle()}>PROJECTS</a></li>
            <li><HashLink smooth to='/#profile'>CONTACT</HashLink></li>
          </ul>
        </div>
      </div>

      {isOpen && (
        <div className='w-full absolute top-20 laptop:hidden'>
          <div className='bg-black'>
            <ul className="items-center flex flex-col laptop:flex-row gap-y-10 py-10 laptop:py-0 laptop:gap-x-20">
              <li><a href='#/' onClick={changeOpen}>HOME</a></li>
              <li><HashLink smooth to='/#about-me' onClick={changeOpen}>ABOUT</HashLink></li>
              <li><HashLink smooth to='/#offerings' onClick={changeOpen}>SERVICES</HashLink></li>
              <li><a href='#/projects' onClick={() => changeProjectToggle()}>PROJECTS</a></li>
              <li><HashLink smooth to='/#profile' onClick={changeOpen}>CONTACT</HashLink></li>
            </ul>
          </div>
        </div>
      )}

    </nav>
  )
}

export default Header
