import ExperiencesModule from "../../modules/ExperiencesModule"
import { experience } from "../../dataset/ExperiencesData"

function MyExperiences() {
  return (
    <div className='h-full'>
      {experience.map((exp) => {
        return(
          <ExperiencesModule duration={exp.duration} company={exp.company} 
        rank={exp.rank} location={exp.location} />
        )
      })}
    </div>
  )
}

export default MyExperiences
