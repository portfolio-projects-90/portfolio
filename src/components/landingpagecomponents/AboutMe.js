import AboutMeStat from "./AboutMeStat";
import { BsDatabase } from "react-icons/bs";
import { LuBook } from "react-icons/lu";
import { HiOutlineUsers } from "react-icons/hi2";
import AboutMeLevel from "./AboutMeLevel";
import { skillSet } from "../../dataset/SkillsData";

function AboutMe() {
  return (
    <div id="about-me" className="grid grid-row laptop:grid-cols-2 mt-10 gap-5 laptop:gap-4 min-h-full items-center">
      <div className="flex flex-col gap-7 min-h-50 laptop:h-50">
        <h1 className="text-4xl font-bold">About Myself</h1>
        <p className="text-base text-justify text-primarybg">As a seasoned Java Developer with 5+ years of experience, I excel in using libraries and frameworks such as Spring Boot, 
        Hibernate, JPA and Postgres. My expertise lies in designing and developing cost-effective 
        high-quality software projects while ensuring they meet the business requirements.</p>
        <div className="grid grid-row-3 laptop:grid-cols-3 gap-10 min-h-20 laptop-h-20">
            <AboutMeStat icon={<BsDatabase />} size={'$2.5M'} title={'Total Donations'} />
            <AboutMeStat icon={<LuBook />} size={'1465'} title={'Total Projects'} />
            <AboutMeStat icon={<HiOutlineUsers />} size={'3965'} title={'Total Volunteers'} />
        </div>
      </div>
        
      <div className="flex flex-col gap-4 laptop:ml-10 min-h-20 my-20 laptop:h-50">
        {skillSet.map((skills) => {
          return(
            <AboutMeLevel level={skills.level} skill={skills.skill} skilllevel={skills.skillLevel} />
          )
        })}
      </div>
    </div>
  )
}

export default AboutMe
