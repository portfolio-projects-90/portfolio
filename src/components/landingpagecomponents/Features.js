import HeaderTextModule from "../../modules/HeaderTextModule"
import FeaturesCode from "./FeaturesCode"
import FeaturesNoCode from "./FeaturesNoCode"
import { useState, useEffect } from "react"
import { useDispatch, useSelector } from 'react-redux'
import { setProjectToggle, setProjectTypeToggle, changeCodeCol, changeNoCodeCol } from '../../redux/slice/ProjectProperties';

function Features() {

    const projectToggle  = useSelector((state) => state.projectProerties.projectToggle);
    const projectTypeToggle  = useSelector((state) => state.projectProerties.projectTypeToggle);
    const codeColValue  = useSelector((state) => state.projectProerties.codeCol);
    const noCodeColValue  = useSelector((state) => state.projectProerties.noCodeCol);
    
    const [toggle, setToggle] = useState(projectTypeToggle);
    const [codeCol, setCodeCol] = useState(codeColValue);
    const [noCodeCol, setNoCodeCol] = useState(noCodeColValue);
    
    const dispatch = useDispatch();

    useEffect(() => {
        setToggle(projectTypeToggle);
        setCodeCol(codeColValue);
        setNoCodeCol(noCodeColValue);
      }, [projectTypeToggle]);
    
    function switchToggle(value){
        setToggle(value);
        dispatch(setProjectTypeToggle(value));
        if(value){
            setCodeCol("text-bggradient1");
            setNoCodeCol("text-black");
            dispatch(changeCodeCol("text-bggradient1"));
            dispatch(changeNoCodeCol("text-black"));
        }else{
            setNoCodeCol("text-bggradient1");
            setCodeCol("text-black");
            dispatch(changeNoCodeCol("text-bggradient1"));
            dispatch(changeCodeCol("text-black"));
        }
    }

    function changeProjectToggle(){
        dispatch(setProjectToggle(true));
    }
    
  return (
    <div className='px-5 py-20 tablet:px-10 laptop:p-40 min-h-full flex flex-col gap-10'>

        <HeaderTextModule title={"OUR LATEST FEATURED PROJECTS"} 
        content={"Who are in extremely love with eco friendly system."} />

        <div className="flex flex-col gap-10 min-h-full pt-20">
            <div className="flex flex-row gap-20 justify-center uppercase font-medium text-sm">
                <p className={"cursor-pointer " + codeCol} onClick={() => switchToggle(true)}>Code</p>
                <p className={"cursor-pointer " + noCodeCol} onClick={() => switchToggle(false)}>No Code</p>
            </div>
            {toggle ? <FeaturesCode /> : <FeaturesNoCode />}
        </div>
        
        <div className="flex flex-col items-center pt-20">
            {projectToggle ? <p></p> :
             <a href='#/projects' onClick={() => changeProjectToggle()} className='w-40 laptop:w-13 h-8 text-center justify-center flex flex-col  text-white rounded-lg text-sm font-medium bg-gradient-to-r from-bggradient1 to-bggradient2'>Load More Items</a>}
        </div>
    </div>
  )
}

export default Features
