import React from 'react'

function AboutMeLevel(props) {
  return (
    <div className='flex flex-col gap-2'>
        <p className='text-base font-medium'>{props.skill} {props.level}%</p>
        <div className='border border-mostborder1 h-3 rounded-3xl bg-white w-full flex flex-row items-center'>
          <div className='h-1 rounded-3xl bg-progressbarbg mx-2 w-full flex flex-row items-center'>
              {/* <div className={'bg-progressbarbg rounded-3xl h-full w-' + props.skilllevel + ' bg-gradient-to-r from-bggradient1 to-bggradient2'}></div> */}
              <div className={'bg-progressbarbg rounded-3xl h-full w-70p bg-gradient-to-r from-bggradient1 to-bggradient2'}></div>
          </div>
        </div>
    </div>
  )
}

export default AboutMeLevel
