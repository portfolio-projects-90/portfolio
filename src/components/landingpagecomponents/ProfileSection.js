import { SlCalender } from "react-icons/sl";
import { CiPhone, CiMail } from "react-icons/ci";
import { AiOutlineHome } from "react-icons/ai";
import { FaFacebookSquare, FaLinkedin } from "react-icons/fa";
import { FaSquareTwitter } from "react-icons/fa6";

function ProfileSection() {
  return (
    <div id="profile" className="bg-gradient-to-r from-bggradient1 to-bggradient2 min-h-85 laptop:h-85 tablet:h-85 laptop:py-20 tablet:pt-10">
        <div className="grid grid-row-2 laptop:grid-cols-3 bg-white relative shadow-profileshadow mx-auto min-h-120 laptop:h-120 w-screen tablet:w-90 laptop:w-80 laptop:rounded-3xl tablet:rounded-3xl">
            <div className="laptop:col-span-2 h-50 tablet:h-50 laptop:h-120 py-10 px-5 tablet:px-10 laptop:px-10 w-screen laptop:w-full tablet:w-full flex flex-row justify-center">
                <img className="h-full w-full tablet:w-60 rounded-3xl" src="https://res.cloudinary.com/ddnmos7xl/image/upload/v1682419126/cooperate%20photo.jpg" alt="Profile Pics" />
            </div>

            <div className="laptop:py-10 text-black mt-10 px-5 laptop:px-0 laptop:mx-10 tablet:mx-24">
                <div className="flex flex-col gap-y-5">
                    <h1 className="text-base laptop:text-sm uppercase">Hello Everybody, I am</h1>
                    <div className="text-3xl laptop:text-5xl uppercase flex flex-col gap-y-3 font-bold">
                        <h1>Abdulmalik</h1>
                        <h1>Osikena</h1>
                        <h1>Abdullahi</h1>
                    </div>
                    <h2 className="text-base uppercase font-medium">Backend Engineer</h2>
                    <p className="text-base text-justify text-primarybg">As a seasoned Java Developer with 5+ years of experience, I excel in using libraries and frameworks such as Spring Boot, 
                    Hibernate, JPA and Postgres. My expertise lies in designing and developing cost-effective 
                    high-quality software projects while ensuring they meet the business requirements.</p>
                </div>
                <ul className="text-base my-10 flex flex-col gap-y-3 laptop:-ml-3 text-primarybg" >
                    <li className="flex flex-row items-center gap-x-2">
                        <SlCalender className="h-5 w-5 laptop:h-3 laptop:w-3" />
                        <p>22nd Feburary</p>
                    </li>
                    <li className="flex flex-row items-center gap-x-2">
                        <CiPhone className="h-5 w-5 laptop:h-3 laptop:w-3" />
                        <p>+2348160921372</p>
                    </li>
                    <li className="flex flex-row items-center gap-x-2">
                        <CiMail className="h-5 w-5 laptop:h-3 laptop:w-3" />
                        <p>aabdulmalik90@yahoo.com</p>
                    </li>
                    <li className="flex flex-row items-center gap-x-2">
                        <AiOutlineHome className="h-5 w-5 laptop:h-3 laptop:w-3" />
                        <p>Ketu, Lagos</p>
                    </li>
                </ul>
                <div className="flex flex-row gap-x-5 mb-10 laptop:gap-x-2 laptop:-ml-3 text-primarybg" >
                    <FaFacebookSquare className="h-5 w-5 laptop:h-3 laptop:w-3" />
                    <FaSquareTwitter className="h-5 w-5 laptop:h-3 laptop:w-3" />
                    <FaLinkedin className="h-5 w-5 laptop:h-3 laptop:w-3" />
                </div>
            </div>
        </div>
    </div>
  )
}

export default ProfileSection
