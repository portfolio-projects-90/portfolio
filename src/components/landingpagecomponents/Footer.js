import { FaArrowRightLong, FaTwitter } from "react-icons/fa6";
import { FaLinkedinIn, FaFacebookF } from "react-icons/fa";

function Footer() {
  return (
    <div className='px-5 py-20 tablet:px-10 laptop:px-40 flex flex-col justify-start laptop:justify-center h-full text-primarybg text-sm'>
        <div className='grid grid-row laptop:grid-cols-5 gap-10'>

            <div className="col-span-2 flex flex-col gap-10">
                <p className="text-white font-bold text-lg">About Me</p>
                <p>Do you want to be even more successful? Learn to love learning and growth. The more effort you put into improving your skills,</p>
                <p>Copyright ©2023 All rights reserved | This template is made with by Colorlib</p>
            </div>

            <div className="col-span-2 flex flex-col gap-10">
                <p className="text-white font-bold text-lg">Newsletter</p>
                <div className="flex flex-col gap-3">
                    <p>Stay updated with our latest trends</p>
                    <div className="flex flex-row items-center h-5 border border-bggradient1 w-full">
                        <input className="pl-2 w-90p h-full bg-black outline-0" type='text' placeholder="Email Address" />
                        <div className="h-full w-10p flex flex-row items-center justify-center cursor-pointer text-white bg-gradient-to-r from-bggradient1 to-bggradient2">
                            <FaArrowRightLong />
                        </div>
                    </div>
                </div>
            </div>

            <div className="flex flex-col gap-10">
                <p className="text-white font-bold text-lg">Follow Me</p>
                <div className="flex flex-col gap-3">
                    <p>Let us be social</p>
                    <div className="flex flex-row gap-5 items-center h-5 text-white">
                        <FaFacebookF />
                        <FaTwitter />
                        <FaLinkedinIn />
                    </div>
                </div>
            </div>

        </div>
    </div>
  )
}

export default Footer
