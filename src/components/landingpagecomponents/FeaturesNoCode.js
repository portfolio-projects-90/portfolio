import FeaturesModule from "../../modules/FeaturesModule"
import { noCodeProjects } from "../../dataset/ProjectsData"
import { useState, useEffect } from "react";
import { useSelector } from 'react-redux'

function FeaturesNoCode() {

  const projectToggle  = useSelector((state) => state.projectProerties.projectToggle);
  const [noCodeProject, setNoCodeProject] = useState(noCodeProjects);

  useEffect(() => {
    selectData();
  }, [projectToggle]);

  function selectData(){
    if(!projectToggle){
      setNoCodeProject(noCodeProjects.slice(0, 3));
    }
  }

  return (
    <div className='min-h-20 grid grid-row-3 laptop:grid-cols-3 gap-10'>
      {noCodeProject.map((noCode) => {
        return(
          <FeaturesModule title={noCode.title} type={noCode.type} code={noCode.code} projectType={"noCode"} 
          projectCoverLink={noCode.projectCoverLink} />
        )
      })}
    </div>
  )
}

export default FeaturesNoCode
