import ExperiencesModule from "../../modules/ExperiencesModule"
import { education } from "../../dataset/ExperiencesData"

function MyEducation() {
  return (
    <div className='min-h-20'>
      {education.map((edu) => {
      return(
        <ExperiencesModule duration={edu.duration} company={edu.company} 
      rank={edu.rank} location={edu.location} />
      )
    })}
    </div>
  )
}

export default MyEducation
