
function OfferingsContent(props) {
  return (
    <div className='flex flex-col gap-5 laptop:gap-7 p-10 bg-white rounded-xl text-primarybg'>
        {props.icon}
        <p className="uppercase text-black text-2xl laptop:text-xl font-bold">{props.title}</p>
        <p className="text-base">{props.content}</p>
    </div>
  )
}

export default OfferingsContent
