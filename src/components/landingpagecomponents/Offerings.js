// import HeaderTextModule from "../modules/HeaderTextModule"
import HeaderTextModule from "../../modules/HeaderTextModule"
import OfferingsContent from "./OfferingsContent"
import { BsBuildings } from "react-icons/bs";

function Offerings() {
  return (
    <div id="offerings" className="px-5 py-20 tablet:px-10 laptop:p-40 h-full flex flex-col gap-20">
        <HeaderTextModule title={"OFFERINGS TO MY CLIENTS"} 
        content={"If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each."} />
        
        <div className="grid grit-row-3 laptop:grid-cols-3 gap-10">
            <OfferingsContent title={"ARCHITECTURE"} icon={<BsBuildings className="h-15 w-15 laptop:h-5 laptop:w-5 laptop:-ml-5" />}
            content={"If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $17 each."} />

            <OfferingsContent title={"INTERIOR DESIGN"} icon={<BsBuildings className="h-15 w-15 laptop:h-5 laptop:w-5 laptop:-ml-5" />}
            content={"If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $17 each."} />

            <OfferingsContent title={"CONCEPT DESIGN"} icon={<BsBuildings className="h-15 w-15 laptop:h-5 laptop:w-5 laptop:-ml-5" />}
            content={"If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $17 each."} />
        </div>
    </div>
  )
}

export default Offerings
