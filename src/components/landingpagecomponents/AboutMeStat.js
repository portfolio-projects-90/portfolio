

function AboutMeStat(props) {
  return (
    <div className='border-2 border-mostborder rounded-md flex flex-col gap-3 p-5'>
      {props.icon}
      <p className="font-bold text-2xl">{props.size} </p>
      <p className="text-primarybg text-base">{props.title}</p>
    </div>
  )
}

export default AboutMeStat
