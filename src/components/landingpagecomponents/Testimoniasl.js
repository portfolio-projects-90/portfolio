import HeaderTextModule from "../../modules/HeaderTextModule"
import TestimoniaslModule from "../../modules/TestimoniaslModule"
import { testimonial } from "../../dataset/TestimoniaslData"

function Testimoniasl() {
  return (
    <div className='px-5 py-20 tablet:px-10 laptop:p-40 h-full w-full flex flex-col gap-20'>

        <HeaderTextModule title={"TESTIMONIALS"} 
        content={"If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each."} />

        <div className="grid grid-row-3 laptop:grid-cols-3 gap-5 overflow-x-auto">
          {testimonial.map((result) => {
            return(
              <TestimoniaslModule title={result.title} 
            content={result.content} />
            )
          })}
        </div>
      
    </div>
  )
}

export default Testimoniasl
