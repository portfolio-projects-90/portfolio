import FeaturesModule from "../../modules/FeaturesModule"
import { codeProjects } from "../../dataset/ProjectsData"
import { useState, useEffect } from "react";
import { useSelector } from 'react-redux'

function FeaturesCode() {

  const projectToggle  = useSelector((state) => state.projectProerties.projectToggle);
  const [codeProject, setCodeProject] = useState(codeProjects);

  useEffect(() => {
    selectData();
  }, [projectToggle]);

  function selectData(){
    if(!projectToggle){
      setCodeProject(codeProjects.slice(0, 3));
    }
  }

  return (
    <div className='min-h-20 grid grid-row-3 laptop:grid-cols-3 gap-10'>
      {codeProject.map((code) => {
        return(
          <FeaturesModule title={code.title} type={code.type} code={code.code} projectType={"code"} 
          projectCoverLink={code.projectCoverLink} />
        )
      })}
      
    </div>
  )
}

export default FeaturesCode
