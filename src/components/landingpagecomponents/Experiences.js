import MyExperiences from "./MyExperiences";
import MyEducation from "./MyEducation";
import { useState } from "react";

function Experiences() {

    const [toggle, setToggle] = useState(true);
    const [expbg, setExpbg] = useState('bg-white text-black');
    const [edubg, setEdubg] = useState('bg-btnbg');

    function clickToggle(value){
        setToggle(value);
        if(value){
            setExpbg('bg-white text-black');
            setEdubg('bg-btnbg');
        }else{
            setEdubg('bg-white text-black');
            setExpbg('bg-btnbg');
        }
    }

  return (
    <div className='min-h-full flex flex-col text-white'>
        <div className='min-h-80 w-full flex flex-col gap-10 laptop:gap-20'>
            <div className='flex flex-row gap-5 laptop:gap-10 h-7 justify-center mt-20'>
                <button className={'w-35 laptop:w-13 rounded-lg text-sm font-medium ' + expbg} onClick={() => clickToggle(true)}>My Experiences</button>
                <button className={'w-35 laptop:w-13 rounded-lg text-sm font-medium ' + edubg} onClick={() => clickToggle(false)}>My Eduction</button>
            </div>
            <div className="min-h-70 pb-20">
                {toggle ? <MyExperiences /> : <MyEducation />}
            </div>
        </div>
    </div>
  )
}

export default Experiences
