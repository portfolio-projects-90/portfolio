import React from 'react'

function TestimoniaslModule(props) {
  return (
    <div className='flex flex-col gap-5 p-10 rounded-xl bg-white'>
        <p className='text-base text-primarybg italic'>{props.content}</p>
        <p className='uppercase text-black font-bold text-xl'>{props.title}</p>
    </div>
  )
}

export default TestimoniaslModule
