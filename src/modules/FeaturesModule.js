import { useNavigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { setProjectCode, setProjectType } from '../redux/slice/ProjectProperties';

function FeaturesModule(props) {

  const navigate = useNavigate();
  const dispatch = useDispatch();

  function switchToProject(){
    dispatch(setProjectCode(props.code));
    dispatch(setProjectType(props.projectType));
    navigate('/projects/learn/more');
  }

  return (
    <div className='min-h-full p-2 laptop:p-0 flex flex-col gap-4 items-center'>
        <img className='h-50 w-full laptop:w-20 rounded-lg' src={props.projectCoverLink} alt='Pic' />
        <div className='flex flex-col items-center gap-2'>
            <p className='text-black font-bold text-xl' onClick={switchToProject}>{props.title}</p>
            <p className='text-primarybg text-base'>{props.type}</p>
        </div>
    </div>
  )
}

export default FeaturesModule
