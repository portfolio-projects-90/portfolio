import React from 'react'

function ExperiencesModule(props) {
  return (
    <div className='text-white flex flex-col tablet:flex-row laptop:flex-row gap-5 tablet:gap-0 laptop:gap-0 min-h-25 py-10 justify-center pl-5 tablet:pl-0 tablet:justify-center laptop:pl-0 laptop:justify-center'>

      <div className='flex flex-col self-center w-full laptop:w-30 tablet:w-30 text-left tablet:text-right laptop:text-right pr-5 text-base'>
        <p>{props.duration}</p>
      </div>

      <div className='bg-btnbg w-7px h-full hidden tablet:flex laptop:flex flex-row justify-center items-center self-center'>
        <div className='w-5px h-7px rounded-full border-2 border-white flex flex-row justify-center items-center'>
          <div className='bg-white w-5px h-5px rounded-full'></div>
        </div>
      </div>

      <div className='flex flex-col gap-5 self-center w-full laptop:w-30 tablet:w-30 tablet:pl-5 laptop:pl-5'>
        <p className='text-lg laptop:text-xl font-bold uppercase'>{props.company}</p>
        <div className='flex flex-col'>
            <p>{props.rank}</p>
            <p>{props.location}</p>
        </div>
      </div>
    </div>
  )
}

export default ExperiencesModule
