import React from 'react'

function HeaderTextModule(props) {
  return (
    <div className='flex flex-col items-center gap-2 laptop:gap-4'>
        <div className='w-full laptop:w-50'>
            <p className='uppercase text-center text-2xl laptop:text-4xl text-black font-bold'>{props.title}</p>
        </div>
        <div className='w-full laptop:w-50'>
            <p className='text-center text-base text-primarybg'>{props.content}</p>
        </div>
        
    </div>
  )
}

export default HeaderTextModule
