import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    projectCode: "",
    projectType: "",
    projectToggle: false,
    projectTypeToggle: true,
    codeCol: "text-bggradient1",
    noCodeCol: "text-black"
}

export const ProjectPropertiesSlice = createSlice({
    name: 'projectProps',
    initialState,
    reducers: {
        setProjectCode: (state, action) => {
            state.projectCode = action.payload
        },
        setProjectType: (state, action) => {
            state.projectType = action.payload
        },
        setProjectToggle: (state, action) => {
            state.projectToggle = action.payload
        }
        ,
        setProjectTypeToggle: (state, action) => {
            state.projectTypeToggle = action.payload
        },
        changeCodeCol: (state, action) => {
            state.codeCol = action.payload
        },
        changeNoCodeCol: (state, action) => {
            state.noCodeCol = action.payload
        }
    }
})

export const { setProjectCode, setProjectType, setProjectToggle, setProjectTypeToggle, changeCodeCol, changeNoCodeCol } = ProjectPropertiesSlice.actions

export default ProjectPropertiesSlice.reducer