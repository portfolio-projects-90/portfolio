import { configureStore } from "@reduxjs/toolkit";
import ProjectPropertiesReducer from "./slice/ProjectProperties";

export const store = configureStore ({
    reducer: {
        projectProerties: ProjectPropertiesReducer
    }
})